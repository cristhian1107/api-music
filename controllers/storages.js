const fs = require('fs');
const { storagesModel } = require('../models');
const { matchedData } = require('express-validator');
const { handleHTTPError } = require('../utils/handle-errors');

const PUBLIC_URL = process.env.PUBLIC_URL;
const MEDIA_PATH = `${__dirname}/../storage`;

/**
 * Obtener toda los registros de la Base de datos.
 * @param {*} req : require.
 * @param {*} res : response.
 */
const getItems = async (req, res) => {
  try {
    // Traer todas las pistas.
    const data = await storagesModel.find({});
    // Mostrar resultados al cliente.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al traer los items');
  }
};

/**
 * Obtener un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const getItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Obtenemos el id.
    const { id } = body;
    // Traer la pista.
    const data = await storagesModel.findById(id);
    // Mostramos la informacion consultada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al traer el item');
  }
};

/**
 * Insertar un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const createItem = async (req, res) => {
  try {
    // Recuperamos la informacion enviada.
    const { file } = req;
    // Creamos un objecto para almacenar la información.
    const fileData = {
      filename: file.filename,
      url: `${PUBLIC_URL}/${file.filename}`
    };
    // Guardamos la información en la base de datos.
    const data = await storagesModel.create(fileData);
    // Mostramos la informacion almacenada.
    res.status(201);
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al guardar el item');
  }
};

/**
 * Eliminar un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const deleteItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Obtenemos el id.
    const { id } = body;
    // Traer la pista.
    const dataFile = await storagesModel.findById(id);
    // Optenemos el filename
    const { filename } = dataFile;
    // Ruta del archivo a eliminar del servidor.
    const filePath = `${MEDIA_PATH}/${filename}`;
    // Eliminamos el archivo del servidor.
    fs.unlinkSync(filePath);
    // Eliminamos el archivo de la base de datos definitivamente.
    await storagesModel.deleteOne({ _id: id });
    // Armanos el objeto a devolver.
    const data = {
      filePath,
      deleted: 1
    };
    // Mostramos la informacion eliminada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al eliminar el item');
  }
};

module.exports = { getItems, getItem, createItem, deleteItem };
