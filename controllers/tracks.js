const { tracksModel } = require('../models');
const { matchedData } = require('express-validator');
const { handleHTTPError } = require('../utils/handle-errors');
const ENGINE_DB = process.env.ENGINE_DB;

/**
 * Obtener toda los registros de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const getItems = async (req, res) => {
  try {
    // Traer todas las pistas.
    // ?const data = await tracksModel.find({});
    const data = await tracksModel.findAllData({});
    // Mostramos la informacion consultada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al traer los items');
  }
};

/**
 * Obtener un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const getItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Obtenemos el id.
    const { id } = body;
    // Traer la pista.
    // ?const data = await tracksModel.findById(id);
    const data = await tracksModel.findOneData(id);
    // Mostramos la informacion consultada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al traer el item');
  }
};

/**
 * Insertar un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const createItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Insertamos el nuevo registro.
    const data = await tracksModel.create(body);
    // Mostramos la informacion guardada.
    res.status(201);
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al guardar el item');
  }
};

/**
 * Actualizar un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const updateItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const { id, ...body } = matchedData(req);
    // Buscamos el item y actualizamos.
    let data;
    if (ENGINE_DB === 'nosql') {
      data = await tracksModel.findOneAndUpdate({ _id: id }, body, { new: true });
    } else {
      const result = await tracksModel.update(body, { where: { id: id } });
      data = (result) ? await tracksModel.findById(id) : undefined;
    }
    // Mostramos la informacion guardada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al actualizar el item');
  }
};

/**
 * Eliminar un registro de la Base de datos.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const deleteItem = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Obtenemos el id.
    const { id } = body;
    // Eliminamos el item.
    const data = await tracksModel.delete({ _id: id });
    // Mostramos informacion eliminada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al eliminar el item');
  }
};

/**
 * Eliminar un registro de la Base de datos permanentemente.
 * @param {*} req : requies.
 * @param {*} res : response.
 */
const deleteItemDefinitely = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    const body = matchedData(req);
    // Obtenemos el id.
    const { id } = body;
    // Eliminamos el item.
    const data = await tracksModel.deleteOne({ _id: id });
    // Mostramos informacion eliminada.
    res.send({ data });
  } catch (e) {
    handleHTTPError(res, 'Error al eliminar el item');
  }
};

module.exports = { getItems, getItem, createItem, updateItem, deleteItem };
