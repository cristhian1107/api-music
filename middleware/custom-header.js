const customHeader = (req, res, next) => {
  try {
    let isCorrect = false;
    // Validamos si existe un cabecera personalizada.
    const apiKey = req.headers['api-key'];
    isCorrect = (apiKey === 'cristhian-node-api');
    // Si no es correcto interrumpimos el proceso.
    if (!isCorrect) {
      res.status(403);
      res.send({ error: 'El api key es incorrecto' });
    } else {
      next();
    }
  } catch (err) {
    res.status(403);
    res.send({ error: 'Ocurrio un error al validar las cabeceras.' });
  }
};

module.exports = customHeader;
