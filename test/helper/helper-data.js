const testAuthLogin = {
  email: 'bill.gates@gmail.com',
  password: '0102-0304'
};

const testAuthRegister = {
  name: 'Bill Gates',
  age: 66,
  email: 'bill.gates@gmail.com',
  password: '0102-0304'
};

const testAuthRegister2 = {
  name: 'Jeff Bezos',
  age: 58,
  email: 'jeff.bezos@gmail.com',
  password: '0102-0304'
};

const testAuthRegisterAdmin = {
  name: 'User admin',
  age: 30,
  email: 'user@admin.com',
  role: ['admin'],
  password: 'admin'
};

const testStorageRegister = {
  url: 'http://localhost:3001/file-test.mp3',
  filename: 'file-test.mp3'
};

const testDataTrack = {
  name: 'Eminem - Without Me (Official Music Video) V3',
  album: 'Eminem',
  cover: 'https://cdns-images.dzcdn.net/images/cover/ec3c8ed67427064c70f67e5815b74cef/350x350.jpg',
  artist: {
    name: 'Eminem',
    nickname: 'Eminem',
    nationality: 'US'
  },
  duration: {
    start: 1,
    end: 3
  },
  mediaId: ''
};

module.exports = {
  testAuthLogin,
  testAuthRegister,
  testAuthRegister2,
  testAuthRegisterAdmin,
  testStorageRegister,
  testDataTrack
};
