const ENGINE_DB = process.env.ENGINE_DB;

/**
 * Escogemos el id segun motor de DB.
 * @returns Modo Sql o NoSql.
 */
const getProperties = () => {
  const data = {
    nosql: {
      id: '_id'
    },
    sql: {
      id: 'id'
    }
  };
  return data[ENGINE_DB];
};

module.exports = getProperties;
