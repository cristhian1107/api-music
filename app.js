require('dotenv').config();
const express = require('express');
const cors = require('cors');
const swaggerUI = require('swagger-ui-express');
const openApiConfigration = require('./docs/swagger');
const morganBody = require('morgan-body');
const loggerStream = require('./utils/handle-logger');
const dbConnectMongo = require('./config/mongo');
const { dbConnectMySql } = require('./config/mysql');
const app = express();

// Tipo de conexión Sql y NoSql.
const ENGINE_DB = process.env.ENGINE_DB;
// Entorno de ejecución.
const NODE_ENV = process.env.NODE_ENV || 'development';

// CORS: Intercambio de recursos de origen cruzado.
app.use(cors());
// Si queremos que nuestra API pueda recibir JSON.
app.use(express.json());
// Publicamos la carpeta storage, para acceder a los archivos.
app.use(express.static('storage'));

// TODO: Stalkear nuestra API.
// Configuramos el morgan para el monitoreo.
morganBody(app, {
  noColors: true, // Quitamos los colores.
  stream: loggerStream, // Envio de mensajes.
  skip: function (req, res) {
    // Solo errores.
    return res.statusCode < 400;
  }
});

// Asignamos el puerto.
const port = process.env.PORT || 3000;

// TODO: Ruta de documentación.
app.use('/documentation', swaggerUI.serve, swaggerUI.setup(openApiConfigration));

// TODO: Aqui estamos gestionando las rutas.
app.use('/api', require('./routes'));

// Iniciamos el servicio.
if (NODE_ENV !== 'test') {
  app.listen(port, () => {
    console.log(`Tu  app esta lista por http://localhost:${port}`);
  });
}

// Nos conectamos a la base de datos.
(ENGINE_DB === 'nosql') ? dbConnectMongo() : dbConnectMySql();

// TODO: Para fines de test.
module.exports = app;
