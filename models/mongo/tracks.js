const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');

const TrackScheme = new mongoose.Schema(
  {
    // Estructura del objeto
    name: {
      type: String
    },
    album: {
      type: String
    },
    cover: {
      type: String,
      validate: {
        validator: (req) => {
          return true;
        },
        message: 'ERROR_URL'
      }
    },
    artist: {
      name: {
        type: String
      },
      nickname: {
        type: String
      },
      nationality: {
        type: String
      }
    },
    duration: {
      start: {
        type: Number
      },
      end: {
        type: Number
      }
    },
    mediaId: {
      type: mongoose.Types.ObjectId
    }
  },
  {
    // Auditoria del objeto.
    timestamps: true,
    versionKey: false
  }
);

/**
 * Implementar metodo propio con relacion a storage.
 * @returns JoinData.
 */
TrackScheme.statics.findAllData = function () {
  const joinData = this.aggregate([
    {
      $lookup: {
        from: 'storages',
        localField: 'mediaId',
        foreignField: '_id',
        as: 'audio'
      }
    },
    {
      // Para solo mostrar un resultado del join.
      $unwind: '$audio'
    }
  ]);
  return joinData;
};

/**
 * Implementar metodo propio con relacion a storage.
 * @param {*} id : Id del tracks a mostrar.
 * @returns JoinData.
 */
TrackScheme.statics.findOneData = function (id) {
  const joinData = this.aggregate([
    {
      $match: {
        _id: mongoose.Types.ObjectId(id)
      }
    },
    {
      $lookup: {
        from: 'storages',
        localField: 'mediaId',
        foreignField: '_id',
        as: 'audio'
      }
    },
    {
      // Para solo mostrar un resultado del join.
      $unwind: '$audio'
    }
  ]);
  return joinData;
};

// Sobreescribir los metodos.
TrackScheme.plugin(mongooseDelete, { overrideMethods: 'all' });
module.exports = mongoose.model('tracks', TrackScheme);
