const request = require('supertest');
const app = require('../app');
const { tokenSign } = require('../utils/handle-jwt');
const { testAuthRegister2 } = require('./helper/helper-data');
const { usersModel } = require('../models');
const { storagesModel } = require('../models');

const filePath = `${__dirname}/dump/track.mp3`;
let JWT_TOKEN = '';
let idStorage = '';

beforeAll(async () => {
  await usersModel.deleteMany({});
  await storagesModel.deleteMany({});
  const user = usersModel.create(testAuthRegister2);
  JWT_TOKEN = await tokenSign(user);
});

describe('[STORAGE] Esta es la prueba para la ruta /api/storages', () => {
  test('Post - Debe almacenar un nuevo archivo', async () => {
    const res = await request(app)
      .post('/api/storages')
      .set('Authorization', `Bearer ${JWT_TOKEN}`)
      .attach('myfile', filePath);
    const { body } = res;
    idStorage = body.data._id;
    expect(res.statusCode).toEqual(201);
    expect(body).toHaveProperty('data');
    expect(body).toHaveProperty('data.url');
  });

  test('GetAll - Debe retornar todos los registros', async () => {
    const res = await request(app)
      .get('/api/storages')
      .set('Authorization', `Bearer ${JWT_TOKEN}`);
    const { body } = res;
    expect(res.statusCode).toEqual(200);
    expect(body).toHaveProperty('data');
  });

  test('GetOne - Debe retornar todo el detalle del item', async () => {
    const { _id } = await storagesModel.findOne();
    id = _id.toString();

    const res = await request(app)
      .get(`/api/storages/${id}`)
      .set('Authorization', `Bearer ${JWT_TOKEN}`);
    const { body } = res;
    expect(res.statusCode).toEqual(200);
    expect(body).toHaveProperty('data');
  });

  test('Delete - Debe eliminar el item', async () => {
    const { _id } = await storagesModel.findOne();
    id = _id.toString();
    const res = await request(app)
      .delete(`/api/storages/${idStorage}`)
      .set('Authorization', `Bearer ${JWT_TOKEN}`);
    const { body } = res;
    expect(res.statusCode).toEqual(200);
    expect(body).toHaveProperty('data');
    expect(body).toHaveProperty('data.deleted', 1);
  });
});
