const bcryptjs = require('bcryptjs');

/**
 * Encripta una constraseña para mayor seguridad.
 * @param {string} passwordPlain: Contraseña sin encriptar.
 * @returns La contraseña encriptada.
 */
const encrypt = async (passwordPlain) => {
  const hash = await bcryptjs.hash(passwordPlain, 10);
  return (hash);
};

/**
 * Compara contraseña con su hash encriptado.
 * @param {string} passwordPlain: Contraseña sin encriptar.
 * @param {string} passwordHash: Constraseña encriptada.
 * @returns True o False.
 */
const compare = async (passwordPlain, passwordHash) => {
  return (await bcryptjs.compare(passwordPlain, passwordHash));
};

module.exports = { encrypt, compare };
