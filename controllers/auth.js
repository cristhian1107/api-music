const { usersModel } = require('../models');
const { matchedData } = require('express-validator');
const { handleHTTPError } = require('../utils/handle-errors');
const { tokenSign } = require('../utils/handle-jwt');
const { encrypt, compare } = require('../utils/handle-password');

/**
 * Registra un nuevo usuario.
 * @param {*} req : require.
 * @param {*} res : response.
 */
const registerCtrl = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    req = matchedData(req);
    // Encriptamos el password.
    const password = await encrypt(req.password);
    // Añadimos el password a objecto User.
    const body = { ...req, password };
    // Almacenamos la informacion an la BD.
    const dataUser = await usersModel.create(body);
    // Removemos el password del objecto para que no se envie.
    dataUser.set('password', undefined, { strict: false });
    // Creamos un nuevo objeto que contiene el Jwt e info del Usuario.
    const data = {
      token: await tokenSign(dataUser),
      user: dataUser
    };
    // Enviamos la información al cliente.
    res.status(201);
    res.send({ data: data });
  } catch (e) {
    handleHTTPError(res, 'Error al registrar el usuario');
  }
};

/**
 * Realiza el login de un usuario.
 * @param {*} req :require.
 * @param {*} res :response.
 */
const loginCtrl = async (req, res) => {
  try {
    // Recuperamos y quitamos los campos demas la informacion enviada.
    req = matchedData(req);
    // Traemos al usuario de la BD.
    // ?const user = await usersModel.findOne({ email: req.email }).select('password name role email');
    const user = await usersModel.findOne({ email: req.email });
    // Nos aseguramos que el usuario existe.
    if (!user) {
      handleHTTPError(res, 'El usuario no existe', 404);
      return;
    }
    // Validamos la contraseña.
    const check = await compare(req.password, user.password);
    // Nos aseguramos que el password es valido.
    if (!check) {
      handleHTTPError(res, 'Password invalido', 401);
      return;
    }
    // Removemos el password del objecto para que no se envie.
    user.set('password', undefined, { strict: false });
    // Si todo esta bien :).
    const data = {
      token: await tokenSign(user),
      user
    };
    // Enviamos la información al cliente.
    res.send({ data: data });
  } catch (e) {
    handleHTTPError(res, 'Error al logear el usuario');
  }
};

module.exports = { registerCtrl, loginCtrl };
