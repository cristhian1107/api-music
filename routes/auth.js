const express = require('express');
const { registerCtrl, loginCtrl } = require('../controllers/auth');
const { validatorRegister, validatorLogin } = require('../validators/auth');

const router = express.Router();

/**
 * Registrar nuevo usuario.
 * @openapi
 * /auth/register:
 *    post:
 *        tags:
 *            - auth
 *        summary: 'Registrar nuevo usuario'
 *        description: 'Esta ruta es para registrar un nuevo usuario'
 *        requestBody:
 *            content:
 *                  application/json:
 *                      schema:
 *                          $ref: "#/components/schemas/authRegister"
 *        responses:
 *            '201':
 *              description: 'Usuario registrado de manera correcta'
 *            '403':
 *              description: 'Error al registrar nuevo usuario - validaciones'
 */
router.post('/register', validatorRegister, registerCtrl);

/**
 * Login de usuario.
 * @openapi
 * /auth/login:
 *    post:
 *      tags:
 *        - auth
 *      summary: "Login user"
 *      description: Iniciar session a un nuevo usuario y obtener el token de sesión
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/authLogin"
 *      responses:
 *        '200':
 *          description: Retorna el objeto insertado en la coleccion.
 *        '422':
 *          description: Error de validacion.
 */
router.post('/login', validatorLogin, loginCtrl);

module.exports = router;
