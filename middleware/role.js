const { handleHTTPError } = require('../utils/handle-errors');

/**
 *
 * @param {*} roles : Array con los roles permitidos.
 * @returns
 */
const checkRole = (roles) => (req, res, next) => {
  try {
    // Obtenemos el usuario.
    const { user } = req;
    // Obtenemos los roles.
    const rolesByUser = user.role;
    // Validamos si el rol esta dentro de lo permitido.
    const checkValueRole = roles.some(
      (rolSingle) => rolesByUser.includes(rolSingle)
    );
    // Si NO cumple con el rol.
    if (!checkValueRole) {
      handleHTTPError(res, 'El usuario no tiene los permisos');
      return;
    }
    // Continuamos...
    next();
  } catch (e) {
    handleHTTPError(res, 'Error de permisos por roles', 403);
  }
};

module.exports = checkRole;
