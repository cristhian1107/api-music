const multer = require('multer');

// Alistamos todo para almacenar un archivo.
const storage = multer.diskStorage({

  destination: function (req, file, cb) {
    // Path de destino del archivo.
    const pathStorage = `${__dirname}/../storage`;

    // Enviamos el archivo al destino.
    cb(null, pathStorage);
  },

  filename: function (req, file, cb) {
    // Obtener la extensión del archivo.
    const ext = file.originalname.split('.').pop();

    // Nombre del archivo segun fecha en formato de numeros.
    const filename = `file-${Date.now()}.${ext}`;

    // Asignamos el nombre al archivo.
    cb(null, filename);
  }
});

// Intercambio de informacion usamos Middleware.
const uploadMiddleware = multer({ storage });

module.exports = uploadMiddleware;
