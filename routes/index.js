const express = require('express');
const fs = require('fs');
const router = express.Router();

// Obtenemos los nombre de los archivos en la ruta actual.
const PATH_ROUTES = __dirname;

// Quitamos las extensiones de los archivos.
const removeExtension = (filename) => {
  return filename.split('.').shift();
};

// Creamos las rutas de manera dinamica.
fs.readdirSync(PATH_ROUTES).filter((file) => {
  const name = removeExtension(file);
  if (name !== 'index') {
    router.use(`/${name}`, require(`./${file}`));
  }
  return (true);
});

module.exports = router;
