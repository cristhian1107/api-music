const { validationResult } = require('express-validator');

/**
 * Segun el resultado de la validacion response al cliente.
 * @param {*} req : requies.
 * @param {*} res : response.
 * @param {*} next : next step.
 * @returns : Continua al controlador si es correcto en caso
 * contrario retorna un error 403.
 */
const validateResults = (req, res, next) => {
  try {
    validationResult(req).throw();
    return next(); // Continua hacia el controlador!.
  } catch (err) {
    res.status(403);
    res.send({ errors: err.array() });
  }
};

module.exports = validateResults;
