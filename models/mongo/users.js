const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');

const UserScheme = new mongoose.Schema(
  {
    // Estructura del objeto
    name: {
      type: String
    },
    age: {
      type: Number
    },
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String
      // ?select: false
    },
    role: {
      type: ['user', 'admin'],
      default: 'user'
    }
  },
  {
    // Auditoria del objeto.
    timestamps: true,
    versionKey: false
  }
);

// Sobreescribir los metodos.
UserScheme.plugin(mongooseDelete, { overrideMethods: 'all' });
module.exports = mongoose.model('users', UserScheme);
