const { sequelize } = require('../../config/mysql');
const { DataTypes } = require('sequelize');
const Storage = require('./storages');

const Tracks = sequelize.define(
  'tracks',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    album: {
      type: DataTypes.STRING
    },
    cover: {
      type: DataTypes.STRING
    },
    artist_name: {
      type: DataTypes.STRING
    },
    artist_nickname: {
      type: DataTypes.STRING
    },
    artist_nationality: {
      type: DataTypes.STRING
    },
    duration_start: {
      type: DataTypes.INTEGER
    },
    duration_end: {
      type: DataTypes.INTEGER
    },
    mediaId: {
      type: DataTypes.STRING
    }
  },
  {
    timestamps: true
  }
);

Tracks.belongsTo(Storage, {
  foreignKey: 'mediaId',
  as: 'audio'
});

/**
 * Implementar metodo propio con relacion a storage.
 * @returns JoinData.
 */
Tracks.findAllData = function () {
  return Tracks.findAll({ include: 'audio' });
};

/**
 * Implementar metodo propio con relacion a storage.
 * @param {*} id : Id del tracks a mostrar.
 * @returns JoinData.
 */
Tracks.findOneData = function (id) {
  return Tracks.findOne({ where: { id }, include: 'audio' });
};

// Sobreescribimos los metodos.
Tracks.find = Tracks.findAll;
Tracks.findById = Tracks.findByPk;

module.exports = Tracks;
