// TODO: Esta nos ayudara a tene run log (monitoreo) de nuestra App.
const { IncomingWebhook } = require('@slack/webhook');

// Nos conectamos a nuestro slack.
const webHook = new IncomingWebhook(process.env.SLACK_WEBHOOK);
// Aqui trabajamos con slack.
const loggerStream = {
  write: message => {
    webHook.send({
      text: message
    });
  }
};

module.exports = loggerStream;
