const jwt = require('jsonwebtoken');
const getProperties = require('../utils/handle-engine');
const JWT_SECRET = process.env.JWT_SECRET;
const propertiesKey = getProperties();

/**
 * Genera un token segun informacion del usuario.
 * @param {object} user: Objeto usuario.
 * @returns: JWT.
 */
const tokenSign = async (user) => {
  try {
    const sign = jwt.sign(
      {
        // _id: user._id,
        [propertiesKey.id]: user[propertiesKey.id],
        role: user.role
      },
      JWT_SECRET,
      {
        expiresIn: '2h'
      }
    );

    return sign;
  } catch (e) {
    return null;
  }
};

/**
 * Valida si un Jwt es valido para nuestra app.
 * @param {string} tokenJwt: Jwt a validar.
 * @returns: Payload.
 */
const tokenVerify = async (tokenJwt) => {
  try {
    return jwt.verify(tokenJwt, JWT_SECRET);
  } catch (e) {
    return null;
  }
};

module.exports = { tokenSign, tokenVerify };
