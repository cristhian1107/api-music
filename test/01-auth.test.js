const request = require('supertest');
const app = require('../app');
const { usersModel } = require('../models');
const { testAuthLogin, testAuthRegister } = require('./helper/helper-data');

beforeAll(async () => {
  await usersModel.deleteMany({});
});

describe('[AUTH] Esta es la prueba para la ruta /api/auth', () => {
  test('Login - Esto deberia de retornar 404', async () => {
    const response = await request(app)
      .post('/api/auth/login')
      .send(testAuthLogin);

    expect(response.statusCode).toEqual(404);
  });

  test('Register - Esto deberia de retornar 201', async () => {
    const response = await request(app)
      .post('/api/auth/register')
      .send(testAuthRegister);

    expect(response.statusCode).toEqual(201);
    expect(response.body).toHaveProperty('data');
    expect(response.body).toHaveProperty('data.token');
    expect(response.body).toHaveProperty('data.user');
  });

  test('Login - Esto deberia de retornar password no valido 401', async () => {
    const newTestAuthLogin = { ...testAuthLogin, password: '0102-0305' };
    const response = await request(app)
      .post('/api/auth/login')
      .send(newTestAuthLogin);

    expect(response.statusCode).toEqual(401);
  });

  test('Login - Esto deberia de retornar 200 login exitoso', async () => {
    const response = await request(app)
      .post('/api/auth/login')
      .send(testAuthLogin);

    expect(response.statusCode).toEqual(200);
  });
});
