const { handleHTTPError } = require('../utils/handle-errors');
const { tokenVerify } = require('../utils/handle-jwt');
const { usersModel } = require('../models');
const getProperties = require('../utils/handle-engine');
const propertiesKey = getProperties();

const authMiddleware = async (req, res, next) => {
  try {
    // Validamos si existe un token.
    if (!req.headers.authorization) {
      handleHTTPError(res, 'No existe token', 401);
      return;
    }
    // Obtenemos el token.
    const token = req.headers.authorization.split(' ').pop();
    // Obtenemos el payload.
    const dataToken = await tokenVerify(token);
    // Validamos payload.
    if (!dataToken) {
      handleHTTPError(res, 'No existe payload', 401);
      return;
    }
    // Armamos un query.
    const query = {
      [propertiesKey.id]: dataToken[propertiesKey.id]
    };
    // Obtenemos el usuario.
    // ?const user = await usersModel.findById(dataToken._id);
    const user = await usersModel.findOne(query);
    // Agregamos al usuario a la peticion.
    req.user = user;
    // Continuamos...
    next();
  } catch (e) {
    handleHTTPError(res, 'Error no session', 401);
  }
};

module.exports = authMiddleware;
