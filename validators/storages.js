const { check } = require('express-validator');
const validateResults = require('../utils/handle-validator');

// Validaciones para listar un item.
const validatorGetItem = [
  // ?check('id').exists().notEmpty().isMongoId(),
  check('id').exists().notEmpty(),

  // respuesta
  (req, res, next) => {
    return validateResults(req, res, next);
  }
];

module.exports = { validatorGetItem };
