const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');

const StorageScheme = new mongoose.Schema(
  {
    // Estructura del objeto
    url: {
      type: String
    },
    filename: {
      type: String
    }
  },
  {
    // Auditoria del objeto.
    timestamps: true,
    versionKey: false
  }
);

// Sobreescribir los metodos.
StorageScheme.plugin(mongooseDelete, { overrideMethods: 'all' });
module.exports = mongoose.model('storages', StorageScheme);
