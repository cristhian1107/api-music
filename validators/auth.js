const { check } = require('express-validator');
const validateResults = require('../utils/handle-validator');

// Validaciones para registrar un nuevo usuario.
const validatorRegister = [
  check('name').exists().notEmpty().isLength({ min: 3, max: 99 }),
  check('age').exists().notEmpty().isNumeric(),
  check('password').exists().notEmpty().isLength({ min: 3, max: 15 }),
  check('email').exists().notEmpty().isEmail(),
  // Resultado.
  (req, res, next) => {
    return validateResults(req, res, next);
  }
];

// Validaciones para login un nuevo usuario.
const validatorLogin = [
  check('password').exists().notEmpty().isLength({ min: 3, max: 15 }),
  check('email').exists().notEmpty().isEmail(),
  // Resultado.
  (req, res, next) => {
    return validateResults(req, res, next);
  }
];

module.exports = { validatorRegister, validatorLogin };
